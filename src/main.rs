use clap::{Command, Arg};
use clap::ColorChoice;
use ids721_week8::run_ps;

fn main() {
    let matches = Command::new("ps")
        .version("0.0.1")
        .author("Zeyu Yuan")
        .about("ps in Rust")
        .color(ColorChoice::Always)
        .arg(
            Arg::new("pid")
                .help("Process to query")
                .required(true)
                .index(1)
        )
        .get_matches();

    if let Some(pid) = matches.get_one::<String>("pid") {
        let output = serde_json::to_string(&run_ps(&pid)).unwrap();
        println!("{}", output);
    } else {
        println!("No process provided");
    }
}

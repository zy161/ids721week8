use std::process::Command;

fn run_command(command: &str) -> String{
    let args: Vec<&str> = command.split_whitespace().collect();
    let output = Command::new(args[0])
        .args(&args[1..])
        .output();
    match output {
        Ok(output) => {
            let stdout = String::from_utf8_lossy(&output.stdout);
            stdout.to_string()
        },
        Err(error) => {
            println!("Command failed: {command}");
            eprintln!("error: {}", error);
            "".to_string()
        }
    }
}


pub fn run_ps(pid: &str) -> serde_json::Value {
    let command = "ps -o pid,ppid,cmd";
    let output = run_command(command);
    if output.is_empty() {
        return serde_json::json!({});
    }
    // Split the output into lines and skip the header
    let mut lines = output.lines();
    if lines.next().is_none() {
        return serde_json::json!({});
    }
    
    // Process each line
    for line in lines {
        // Split the line into fields
        let fields: Vec<&str> = line.trim().split_whitespace().collect();
        
        // Check if the first field matches the provided PID
        if let Some(current_pid) = fields.get(0) {
            if *current_pid == pid {
                // Construct a JSON object representing the process
                return serde_json::json!({
                    "pid": current_pid,
                    "ppid": fields.get(1).unwrap_or(&""),
                    "cmd": fields.get(2..).unwrap_or(&[""]).join(" "),
                });
            }
        }
    } 
    serde_json::json!({})
}

// Unit tests
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_run_command() {
        // Test a successful command
        let output = run_command("echo hello");
        assert_eq!(output.trim(), "hello");

        // Test a failing command
        let output = run_command("nonexistent_command");
        assert!(output.is_empty());
    }

    #[test]
    fn test_run_ps() {
        // Mock output of ps command
        //let ps_output = "10 9 -bash";

        // Test with an existing PID
        let pid_10 = run_ps("10");
        assert_eq!(
            pid_10,
            serde_json::json!({
                "pid": "10",
                "ppid": "9",
                "cmd": "-bash"
            })
        );

        // Test with a non-existing PID
        let non_existing_pid = run_ps("999");
        assert_eq!(non_existing_pid, serde_json::json!({}));

        // Test with an empty output
        let empty_output = serde_json::json!({});
        assert_eq!(run_ps(""), empty_output);
    }
}
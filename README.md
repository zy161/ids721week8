# IDS721 Week8 Project

This project is part of the IDS721 course and focuses on building a Rust command-line tool.

## Initialize a New Cargo Project

The goal of this project is to develop a Rust command-line tool that performs data ingestion/processing and provides unit testing.
1. Use command `cargo new ids721-week8` to create a new project
2. Update the `Cargo.toml` file with required dependencies


## Implement ps Command in `lib.rs` and `main.rs`

In this project, I'm using `ps` command as my rust command-line tool command. In `lib.rs`, implement a function: 

```rust
pub fn run_ps(pid: &str) -> serde_json::Value {
    let command = "ps -o pid,ppid,cmd";
    let output = run_command(command);
    if output.is_empty() {
        return serde_json::json!({});
    }
    // Split the output into lines and skip the header
    let mut lines = output.lines();
    if lines.next().is_none() {
        return serde_json::json!({});
    }
    
    // Process each line
    for line in lines {
        // Split the line into fields
        let fields: Vec<&str> = line.trim().split_whitespace().collect();
        
        // Check if the first field matches the provided PID
        if let Some(current_pid) = fields.get(0) {
            if *current_pid == pid {
                // Construct a JSON object representing the process
                return serde_json::json!({
                    "pid": current_pid,
                    "ppid": fields.get(1).unwrap_or(&""),
                    "cmd": fields.get(2..).unwrap_or(&[""]).join(" "),
                });
            }
        }
    } 
    serde_json::json!({})
}
```

Next, we should update the `main` function in `main.rs`:

```rust
fn main() {
    let matches = Command::new("ps")
        .version("0.0.1")
        .author("Zeyu Yuan")
        .about("ps in Rust")
        .color(ColorChoice::Always)
        .arg(
            Arg::new("pid")
                .help("Process to query")
                .required(true)
                .index(1)
        )
        .get_matches();

    if let Some(pid) = matches.get_one::<String>("pid") {
        let output = serde_json::to_string(&run_ps(&pid)).unwrap();
        println!("{}", output);
    } else {
        println!("No process provided");
    }
}
```

We can start running our comman-line tool with `cargo run` now: 

![ExecutionCLI](img/result.png)

## Add Unit Test into the Application

To add unit test into our application, we have to use `#[cfg(test)]` and `mod tests`: 

```rust
let collection_name = "test_collection";
    // Create a collection with 4-dimensional vectors
    client
    .create_collection(&CreateCollection {
        collection_name: collection_name.to_string(),
        vectors_config: Some(VectorsConfig {
            config: Some(Config::Params(VectorParams {
                size: 4,
                distance: Distance::Dot.into(),
                ..Default::default()
            })),
        }),
        ..Default::default()
    })
    .await?;
```

After adding the test code block, we can use `cargo test` to execute testing:

![TestResult](img/test.png)